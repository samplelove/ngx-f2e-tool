import { Component } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { NestedTreeControl } from '@angular/cdk/tree';
import { ComponentsDatabase } from './example-data';

/** File node data with possible child nodes. */
export interface ComponentNode {
  name: string;
  interfaces?: ComponentInterface[];
  emit?: { [key: string]: any }[];
  state?: { [key: string]: any }[];
  children?: ComponentNode[];
}

export interface ComponentInterface {
  name: string;
  content: { [key: string]: any };
}

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.scss'],
  providers: [ComponentsDatabase]
})
export class TreeViewComponent {
  treeControl: NestedTreeControl<ComponentNode>;
  dataSource: MatTreeNestedDataSource<ComponentNode>;

  constructor(componentsDatabase: ComponentsDatabase) {
    this.treeControl = new NestedTreeControl<ComponentNode>(this.getChildren);
    this.dataSource = new MatTreeNestedDataSource();

    componentsDatabase.dataChange.subscribe(data => this.dataSource.data = data);
  }

  hasChild(index: number, node: ComponentNode) {
    return node.children;
  }

  getChildren(node: ComponentNode) {
    return node.children;
  }

  addInterface(node: ComponentNode, emitEvent: ComponentInterface) {
    node.interfaces.push(emitEvent)
  }
}
