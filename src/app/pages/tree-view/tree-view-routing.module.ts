import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutsComponent } from 'src/app/core/layouts/layouts.component';
import { TreeViewComponent } from './tree-view.component';
import { LayoutsModule } from 'src/app/core/layouts/layouts.module';

const routes: Routes = [
  {
    path: '',
    component: TreeViewComponent,
  }
];

@NgModule({
  imports: [
    LayoutsModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TreeViewRoutingModule { }
