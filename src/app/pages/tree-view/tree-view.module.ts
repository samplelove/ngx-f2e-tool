import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TreeViewRoutingModule } from './tree-view-routing.module';
import { TreeViewComponent } from './tree-view.component';
import { NgMaterialModule } from 'src/app/core/ng-material/ng-material.module';
import { CoreModule } from 'src/app/core/core.module';

@NgModule({
  declarations: [TreeViewComponent],
  exports: [
    TreeViewComponent,
  ],
  imports: [
    CommonModule,
    CoreModule,
    TreeViewRoutingModule,
  ]
})
export class TreeViewModule { }
