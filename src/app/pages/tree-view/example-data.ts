import { ComponentNode as IComponentNode, ComponentInterface } from './tree-view.component';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export const dataObject: ComponentNode = {
  name: "App",
  children: [
    {
      name: "ConfirmModal",
      interfaces: [
        {
          name: "ModalEvent",
          content: {
            descriptions: {
              title: "string",
              content: "string",
            },
            isOpen: "boolean",
          }
        }
      ],
      state: [
        {
          isOpen: true,
        }
      ],
      emit: [
        {
          name: "ok",
          event: {
            data: "gg123456"
          },
        }
      ],
      children: [
        {
          name: "child",
          emit: [
            {
              selected: "number"
            }
          ]
        }
      ]
    }
  ]
};

class ComponentNode implements IComponentNode {
    name: string;
    interfaces?: ComponentInterface[];
    emit?: { [key: string]: any }[];
    state?: { [key: string]: any }[];
    children?: ComponentNode[];

  constructor(node: IComponentNode) {
    this.name = node.name;
    this.interfaces = node.interfaces;
    this.emit = node.emit;
    this.state = node.state;
    this.children = node.children;
  }
}
@Injectable()
export class ComponentsDatabase {
  public dataChange = new BehaviorSubject<ComponentNode[]>([]);

  get data(): ComponentNode[] { return this.dataChange.value; }

  constructor() {
    this.initialize();
  }

  initialize() {
    this.dataChange.next([dataObject]);
  }
}

