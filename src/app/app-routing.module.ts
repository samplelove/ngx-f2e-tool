import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutsComponent } from './core/layouts/layouts.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home',
  },
  {
    component: LayoutsComponent,
    path: '',
    children: [
      {
        loadChildren: './pages/home/home.module#HomeModule',
        path: 'home',
      },
      {
        loadChildren: './pages/tree-view/tree-view.module#TreeViewModule',
        path: 'tree-view',
      }
    ]
  }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes, {
    enableTracing: true,
    useHash: true,
  })],
})
export class AppRoutingModule {}
