import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import JSONEditor from 'jsoneditor';

@Component({
  selector: 'app-interface',
  templateUrl: './interface.component.html',
  styleUrls: ['./interface.component.scss']
})
export class InterfaceComponent implements OnInit {
  @ViewChild('json') json: ElementRef;
  @Input() public interface: any;

  constructor() {}

  ngOnInit() {
    const editor = new JSONEditor(this.json.nativeElement, {
      mode: "text",
    });
    editor.set(this.interface.content);
  }

}
