import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ComponentNode } from 'src/app/pages/tree-view/tree-view.component';
import { ComponentChildList } from 'src/app/core/enum/component-child-list.enum';


@Component({
  selector: 'app-component-detail',
  templateUrl: './component-detail.component.html',
  styleUrls: ['./component-detail.component.scss']
})
export class ComponentDetailComponent implements OnInit {
  @Input() node: ComponentNode;
  @Output() add = new EventEmitter();

  childList = ComponentChildList;

  isInterfaceExpand: boolean = false;
  isStateExpand: boolean = false;
  isEmitExpand: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  addChild(type: ComponentChildList) {
    console.log("addChild: ", type);
    switch(type) {
      case ComponentChildList.Interface:
        this.add.emit({
          name: "newInterface",
          content: {

          }
        });
        break;
      default:
        break;
    }
  }

}
