import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import JSONEditor from 'jsoneditor';

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.scss']
})
export class StateComponent implements OnInit {

  @ViewChild('json') json: ElementRef;

  @Input() state: { [key:string]: any };

  constructor() { }

  ngOnInit() {
    var editor = new JSONEditor(this.json.nativeElement, {
      mode: "text",
    });
    editor.set(this.state);
  }

}
