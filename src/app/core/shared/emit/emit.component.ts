import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import JSONEditor from 'jsoneditor';

@Component({
  selector: 'app-emit',
  templateUrl: './emit.component.html',
  styleUrls: ['./emit.component.scss']
})
export class EmitComponent implements OnInit {
  @ViewChild("json") json: ElementRef;
  @Input() emit: { [key:string]: any };
  constructor() { }

  ngOnInit() {
    const editor = new JSONEditor(this.json.nativeElement, {
      mode: "text",
    });
    editor.set(this.emit);
  }

}
