import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutsComponent } from './layouts.component';
import { RouterModule } from '@angular/router';
import { NgMaterialModule } from '../ng-material/ng-material.module';

@NgModule({
  declarations: [LayoutsComponent],
  exports: [LayoutsComponent],
  imports: [
    CommonModule,
    RouterModule,
    NgMaterialModule,
  ],
})
export class LayoutsModule { }
