import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutsModule } from './layouts/layouts.module';
import { NgMaterialModule } from './ng-material/ng-material.module';
import { InterfaceComponent } from './shared/interface/interface.component';
import { ComponentDetailComponent } from './shared/component-detail/component-detail.component';
import { EmitComponent } from './shared/emit/emit.component';
import { StateComponent } from './shared/state/state.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutsModule,
    NgMaterialModule,
  ],
  exports: [
    LayoutsModule,
    NgMaterialModule,
    ComponentDetailComponent
  ],
  declarations: [ComponentDetailComponent, InterfaceComponent, EmitComponent, StateComponent],
})
export class CoreModule { }
